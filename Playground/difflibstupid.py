import difflib

words = ['hello', 'Hallo', 'hi', 'house', 'key', 'screen', 'hallo', 'question', 'format']
derp = difflib.get_close_matches('Hello', words)
derp2 = difflib.SequenceMatcher(None, 'Hello', words).ratio()
derp3 = [difflib.SequenceMatcher(None, 'Hello', x).ratio() for x in words]

print derp
print derp2
print derp3