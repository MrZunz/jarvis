import time
import serial
import string
import threading

class OBD2Manager(threading.Thread):
        
    def __init__(self):
        print("OBD2Manager:Init()")
        threading.Thread.__init__(self)
        
        self.speed = 0
        #self.connect()
        
        #while True:
        #    input = raw_input("Enter command: ")
            
        #    self.send_command(input)
            
        #    result = self.get_result()
        #    print result
            
            #if (result == "NO DATA" or result.endswith("NO DATA")):
                #self.init()
                
        #    self.parse_result(result)
            
    def run(self):
        #self.connect()
        
        # Main loop
        while True:
            time.sleep(0.5)
            
            self.speed += 0.1

    def connect(self):
        try:
            self.port = serial.Serial( "/dev/rfcomm1", baudrate=38400, timeout=1 )
        
        except serial.SerialException as e:
            print e
            return none
            
        try:
            # Do this to read shit that is in the buffer somehow (like the "DISCONNECTED" (I guess) message)
            print(self.get_result())
            print(self.get_result())
            
            self.init()
            
        except serial.SerialException as e:
            print e
            return none
    
    def get_speed(self):
        return self.speed
        
    def get_rpm(self):
        return "1000"
        
    def init(self):
        
        # Send ATZ to reset the device, this should return ELM device name
        self.send_command("ATZ") #init / reset device
        #time.sleep(0.2)
        print(self.lastcommand, " response: ", self.get_result())
        
        # Set it to the correct protocol
        self.send_command("ATSP3")
        #time.sleep(0.2)
        print(self.lastcommand, " response: ", self.get_result())
        
        # Make it not echo the pid infront of the response, so we can parse next command better
        self.send_command("ATE0") # echo OFF
        #time.sleep(0.2)
        print(self.lastcommand, " response: ", self.get_result())
        
        # Do a slow init, if this response is "BUS INIT: ...OK" the device initialized correctly
        self.send_command("ATSI") # slow (as shit) init

        #time.sleep(1)
        initmsg = self.get_result()
	print(self.lastcommand, " response: ", initmsg)
        if(initmsg == "BUS INIT: ...OK"):
            self.initisok = True
            print("Initialized correctly!")
        else:
            self.initisok = False
            
            # Do some recursion shit till it does get initialized correctly.
            print("Did not initialize correctly, is the battery light burning on the dash?")
            print("trying to initialize...")

            # Wheeeeee
            self.init()
            return
        
        # Make it echo the pid infront of the response
        self.send_command("ATE1") # echo ON
        print(self.lastcommand, " response: ", self.get_result())

    def send_command(self, cmd):
        """Internal use only: not a public interface"""
        if self.port:
            self.port.flushOutput()
            self.port.flushInput()
            for c in cmd:
                self.port.write(c)
            self.port.write("\r\n")
            
            self.lastcommand = cmd
            
    def get_result(self):
        """Internal use only: not a public interface"""
        repeat_count = 0
        if self.port is not None:
            buffer = ""
            while 1:
                c = self.port.read(1)
                if len(c) == 0:
                    if(repeat_count == 5):
                        break
                    #print "Got nothing\n"
                    repeat_count = repeat_count + 1
                    continue
                    
                if c == '\r':
                    continue
                    
                if c == ">":
                    break;
                     
                if buffer != "" or c != ">": #if something is in buffer, add everything
                    buffer = buffer + c
                    

            if(buffer == ""):
                return None
                
            #if not (buffer.startswith(self.lastcommand)):
            #    print("UNEXPECTED DATA: ", buffer)
            #    return None
            # else:
            return buffer
            
        return None
        
    def parse_result(self, result):
        
        result = string.split(result, "\r")
        result = result[0]
         
        # Get the PID and code
        pid = result[:4]
        code = result.replace(self.lastcommand, "")
        
        #remove whitespace
        code = string.split(code)
        code = string.join(code, "")
        
        # first 4 characters are code from ELM
        code = code[4:]
        
        if(pid == "010D"): 
            speed = self.speed(code)
            print 'Speed: ', speed, 'km/h'
        
        if(pid == "010C"):
            rpm = self.rpm(code)
            print 'RPM: ', rpm
    
    def hex_to_int(self, str):
        i = eval("0x" + str, {}, {})
        return i
        
    def rpm(self, code):
        code = self.hex_to_int(code)
        return code / 4
        
    def speed(self, code):
        code = self.hex_to_int(code)
        return code / 1.609

OBD2Manager()
