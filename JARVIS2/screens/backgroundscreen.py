import pygame
from screen import *

class BackgroundScreen(Screen):
    
    def __init__(self, surface):
        print("BackgroundScreen:__init__")
        Screen.__init__(self, "Background")
        self.surface = surface
        self.CurrentBackground = pygame.image.load("screens/images/backgrounds/bg1.png").convert_alpha(surface)
        self.surface.blit(self.CurrentBackground, (0, 0))
        
    def draw(self):
        pass