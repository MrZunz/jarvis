import pygame
from screen import *
#from datetime import datetime
from obd2manager import OBD2Manager
import time

class HomeScreen(Screen):
    
    def __init__(self, surface):
        print("HomeScreen:__init__")
        Screen.__init__(self, "Home")
        self.surface = surface
        self.font = pygame.font.SysFont("Myriad Pro", 40)
        self.topbar = pygame.image.load("screens/images/top-bar.png").convert_alpha(surface)
        self.block = pygame.image.load("screens/images/block-transparent.png").convert_alpha(surface)
        #self.iconCar = pygame.image.load("screens/images/icons/car-128.png").convert_alpha(surface)
        self.iconNote = pygame.image.load("screens/images/icons/note-128.png").convert_alpha(surface)
        
        # We only have to draw this once since the screen wont update anyway
        self.button1 = self.surface.blit(self.block, (35, 75))
        self.button2 = self.surface.blit(self.block, (230, 75))
        self.button3 = self.surface.blit(self.block, (425, 75))
        
        # Only need to draw it once?
        self.surface.blit(self.topbar, (0, 0))
        
        self.obd2 = OBD2Manager()
        self.obd2.start()
        
    def draw(self): 
        self.drawElements()
        self.drawIcons()
        
    def update(self):
        #self.currenttime = str(datetime.now().time())  
        self.currenttime = time.strftime("%H:%M")
        
        self.timelabel = self.font.render(self.currenttime, 1, (255,255,255))
        self.namelabel = self.font.render("JARVIS", 1, (255,255,255))
        self.speedlabel = self.font.render(self.obd2.get_speed(), 1 (255, 255, 255))
        #self.mouselabel = self.font.render(str(pygame.mouse.get_pos()), 1, (255,255,255))
        self.mouselabel = self.font.render(str(pygame.mouse.get_pressed()), 1, (255,255,255))
        
        self.checkPress()
        #pass
        
    def checkPress(self):
        # proceed events
        
        if pygame.mouse.get_pressed()[0] == 1:
            
            if self.button1.collidepoint(pygame.mouse.get_pos()):
                print "HEY YO"
        
    
    def drawElements(self):
        
        # Draw the background
        self.surface.blit(self.topbar, (0, 0))
        
        # DRAW DEBUG MOUSE POS
        self.surface.blit(self.mouselabel, (255, 6))
        
        #self.button1 = self.surface.blit(self.block, (35, 75))
        
        # Draw time in the top right corner of the bar
        self.surface.blit(self.timelabel, (555, 6))
        
        # Draw name in the top left corner of the bar
        self.surface.blit(self.namelabel, (20, 6))
        
        # Draw speed test
        self.surface.blit(self.speedlabel, (20, 100))
        
        # Draw block #1
        #self.surface.blit(self.block, (35, 75))
        
        # Draw block #2
        #self.surface.blit(self.block, (230, 75))
        
        # Draw block #3
        #self.surface.blit(self.block, (425, 75))
        
    def drawIcons(self):
        # Icon #1 (car)
        #self.surface.blit(self.iconCar, (58.5, 99))
        pass