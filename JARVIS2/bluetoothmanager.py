import bluetooth

class BluetoothManager():
    
    def Connect(self, name):

        nearby_devices = bluetooth.discover_devices()
        
        for address in nearby_devices:
            devicename = bluetooth.lookup_name( address )
            print address, devicename
            
            if devicename == name:
                sock=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
                sock.connect((address, 3))
            
bt = BluetoothManager()
bt.Connect("Potato")

