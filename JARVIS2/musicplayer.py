from mpd import MPDClient

class MusicPlayer:
    
    def __init__(self):
        self.client = MPDClient()               # create client object
        self.client.timeout = 10                # network timeout in seconds (floats allowed), default: None
        self.client.idletimeout = None          # timeout for fetching the result of the idle command is handled seperately, default: None
        self.client.connect("localhost", 6600)  # connect to localhost:6600
        
        self.GetPlaylist()
        
        self.client.close()                     # send the close command
        self.client.disconnect()                # disconnect from the server
    
    def Play(self):
        #client.
        print("MusicPlayer:Play()")
        
    def Pause(self):
        if self.client.status()['state'] == "play":
            self.client.pause(1)
        print("MusicPlayer:Pause()")
        
    def Resume(self):
        if self.client.status()['state'] == "pause":
            self.client.pause(0)
        print("MusicPlayer:Resume()")
        
    def Stop(self):
        print("MusicPlayer:Stop()")
        
    def Next(self):
        self.client.next()
        print("MusicPlayer:Next()")
        
    def Previous(self):
        self.client.previous()
        print("MusicPlayer:Previous()")
        
    def GetPlaylist(self):
        print(self.client.listplaylists())
        print("MusicPlayer:GetPlaylist()")

MusicPlayer = MusicPlayer()