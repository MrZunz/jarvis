import collections

class ScreenManager():
    """ creates screens, and makes transitions between screens. """
    
    screens = collections.OrderedDict()
    
    def __init__(self):
        print("ScreenManager:Init()")
    
    def add(self, name, screen):
        self.screens[name] = screen
        
    def update(self):
        for name, screen in self.screens.items():
            #print name, screen
            screen.update()
            
    def draw(self):
        for name, screen in self.screens.items():
            #print name, screen
            screen.draw()