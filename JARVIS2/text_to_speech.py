import os
import glob
import urllib
import urllib2
import threading

from subprocess import Popen

class TextToSpeech(threading.Thread):
    
    def __init__(self, text):
        threading.Thread.__init__(self)
        self.text = text
        print "TextToSpeech:Init()"
        
    def run(self):
        self.say(self.text)
        pass
    
    def say(self, text):
        
        # Create temp directory if it doesn't exist yet
        tempdirectory = os.path.join("temp")
        print "temp directory is at: " + tempdirectory
        if not os.path.exists(tempdirectory):
            print "temp directory did not exist yet, created it at " + tempdirectory
            os.makedirs(tempdirectory)
              
        # Split the text to say into chunks of 100 characters since that is the max amount google will handle.
        for x in self.split_by_n(text,100):
     
            filename = x + ".mp3"
            filepath = os.path.join(tempdirectory, filename)
            
            if os.path.isfile(filepath):
                print "File already found in temp folder, playing that one instead."
                           
            else:
                print "File hasn't been found. Attempting to get it from google now..."
                headers = { 'User-Agent' : 'Mozilla/5.0' }
                req = urllib2.Request("http://translate.google.com/translate_tts?tl=en&q=" + urllib.quote_plus(x) , None, headers)
                response = urllib2.urlopen(req)
         
                print "Saving..."
                output = open(filepath, 'w')
                output.write(response.read())
                output.close()
            
            print "Playing audio file: " + filepath
            p = Popen(["mpg321", "-q", filepath])
            p.communicate()
            print "Done playing audio file"
    
    
    def split_by_n(self, seq, n ):
        #A generator to divide a sequence into chunks of n units.
        while seq:
            yield seq[:n]
            seq = seq[n:]