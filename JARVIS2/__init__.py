import pygame
from screenmanager import ScreenManager
from text_to_speech import TextToSpeech
from obd2manager import OBD2Manager

#from screens import *
from screens.homescreen import *
from screens.backgroundscreen import *

class JARVIS:
    
    def __init__(self):
        print("JARVIS:__init__")
        
        #TextToSpeech("Hello sir.").start()
        
        #self.obd2 = OBD2Manager()
        #self.obd2.start()
        
        #self.texttospeech.say("Hello sir");
        
        
        
        pygame.init()
        self.surface = pygame.display.set_mode((640,480), pygame.FULLSCREEN)
        self.clock = pygame.time.Clock()
        self.screenmanager = ScreenManager() 
        self.screenmanager.add("Background", BackgroundScreen(self.surface))
        self.screenmanager.add("Home", HomeScreen(self.surface))
        #self.ScreenManager.Add("Home", HomeScreen())
        
        self.MainLoop()
        
    def MainLoop(self):
        while True:
            # Limit to 25 FPS
            time_passed = self.clock.tick(25)
            
            #print "Speed: ", self.obd2.get_speed()
        
            # Subscribe to some events
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                
                if event.type == pygame.KEYDOWN :    
                    if event.key == pygame.K_ESCAPE:
                        pygame.quit()
                        sys.exit()
            
            # Update the screens
            self.screenmanager.update()
            self.screenmanager.draw()
            
            pygame.display.flip()
           
           
JARVIS()

