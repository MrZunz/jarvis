# Just A Rarely Very Intelligent System

import os
import pygame
import sys
from brain import Brain

pygame.init()

print pygame.display.list_modes()

screen = pygame.display.set_mode((784,480),pygame.FULLSCREEN)
clock = pygame.time.Clock()
background = pygame.image.load("_userinterface/Images/background6.png").convert()
circle = pygame.image.load("_userinterface/Images/transparent-circle.png").convert_alpha()
myFont = pygame.font.SysFont("arial", 30) 

screen.blit(background, (0,0)) 
screen.blit(circle, (300, 140))
pygame.display.flip()

brain = Brain()

#brain.StartListening()
    
while True:
    
    try:
        brain.Ears.Listen()
    except KeyboardInterrupt:
        print "Keyboard interrupt."
        sys.exit()
    except:
        print "Didn't understand anything."
        
    #Limit to 60 FPS
    time_passed = clock.tick(60)

    # subscribe to some events
    for event in pygame.event.get():
            if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
    
    #if(brain.State == "Listening"):
    #    print brain.State
    

    #print("JARVIS. FPS: %.2f" % (clock.get_fps()))
    #pygame.draw.rect(screen, (0, 128, 255), pygame.Rect(30, 30, 60, 60))
    pygame.display.flip()