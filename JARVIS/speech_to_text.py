import os
import httplib
import json
import sys

def speech_to_text(audio):
    
    Brain.State = "Thinking"
    print Brain.State
    
    url = "www.google.com"
    path = "/speech-api/v1/recognize?xjerr=1&client=chromium&lang=en"
    headers = { "Content-type": "audio/x-flac; rate=16000" };
    params = {"xjerr": "1", "client": "chromium"}
    conn = httplib.HTTPSConnection(url)
    conn.request("POST", path, audio, headers)
    response = conn.getresponse()
    data = response.read()
    
    try:      
        jsdata = json.loads(data)
        print jsdata
        return jsdata["hypotheses"][0]["utterance"]
    except:
        raise Exception, "Could not find any words" 