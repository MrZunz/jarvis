import sys

from ears import Ears
from vocabulary import Vocabulary

class Brain:
    
    State = "Idle"
    
    def __init__(self):
        print "Brain: __init__"
        
        State = "Init"    
        print Brain.State
        
        self.Ears = Ears()
        self.Vocabulary = Vocabulary()

        #self.StartListening()        

    def StartListening(self):
        try:
            heard = self.Ears.Listen()
            print "Brain heard: ", heard
            
            if(heard == "Jarvis"):
                print "HE SAID THE MAGIC WORD!"
            else:
                self.StartListening()

        except KeyboardInterrupt:
            print "Keyboard interrupt."
            sys.exit()
            
        except:
            print "Error, trying again."
            self.StartListening()


# For testing purposes only!
#brain = Brain()