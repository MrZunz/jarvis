import os
import subprocess
import time
import speech_to_text
import brain

class Ears:
    
    tempfile = "out.wav"
    tempfileconverted = "out.flac"
    
    def __init__(self):
        print "Ears: __init__"
        
    def Listen(self):
        #print "Ears: listening"
        
        brain.State = "Listening"        
        print brain.State
        
        # Delete previous recorded file
        if(os.path.exists(self.tempfile)):
            #print 'Deleting temp file: ', self.tempfile
            os.remove(self.tempfile)

        if(os.path.exists(self.tempfileconverted)):
            #print 'Deleting temp converted file: ', self.tempfileconverted
            os.remove(self.tempfileconverted)

        cmd = 'sox -q -r 16000 -t alsa "plughw:1,0" out.wav silence 1 0.5 1% 1 2 1%'
        
        # Open sox so it records input
        p1 = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, shell=True)
        time.sleep(1)
        
        while True:
            filesize1 = os.path.getsize(self.tempfile)
            time.sleep(1)  
            filesize2 = os.path.getsize(self.tempfile)

            # Check if sox has picked up sound and has appended that to the file
            if(filesize1 != filesize2):
                print "Sound detected!"
                
                # Start another loop to detect when silence occured
                while True:
                    filesize1 = os.path.getsize(self.tempfile)
                    time.sleep(1)                 
                    filesize2 = os.path.getsize(self.tempfile)
                    
                    # Check if file sizes didn't change, if so, it must've been silence
                    if(filesize1 == filesize2):
                        print "Silence detected!"
                         
                        brain.State = "Thinking"
                        print brain.State
                        
                        #convert to 16000 bitrate   
                        print "Converting to 16000 bitrate"
                        cmd = 'sox ' + self.tempfile + " " +  self.tempfileconverted +' rate 16k'
        
                        # Open the process to convert to flac and make sure we block the process till it's done
                        p2 = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, shell=True)
                        p2.communicate()
                        
                        print "Attempting to get text from speech..."
                       
                        try:
                            audio = open(self.tempfileconverted, 'r')
                            stt = speech_to_text.speech_to_text(audio)
                            print "You said: " + stt
                            return stt
                        except:
                            raise Exception, "Didn't understand anything."
                        
                        # Make sure we break out of the silence detection loop
                        break
                    
                # Make sure we break out of the sound detection loop
                break