import pygame

pygame.init()

print pygame.display.list_modes()

screen = pygame.display.set_mode((800,600), pygame.RESIZABLE)
clock = pygame.time.Clock()
background = pygame.image.load("Images/background5.jpg").convert()
circle = pygame.image.load("Images/transparent-circle.png").convert_alpha()
myFont = pygame.font.SysFont("arial", 30) 

screen.blit(background, (0,0)) 
screen.blit(circle, (300, 140))
    
while True:
    
    #Limit to 60 FPS
    time_passed = clock.tick(60)

    # subscribe to some events
    for event in pygame.event.get():
            if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
    
    pygame.draw.rect(screen, (0, 128, 255), pygame.Rect(30, 30, 60, 60))
    pygame.display.flip()