import os
import glob
import urllib
import urllib2

from subprocess import Popen

def say(text):
    
    # Create temp directory if it doesn't exist yet
    tempdirectory = os.path.join("temp")
    if not os.path.exists(tempdirectory):
        os.makedirs(tempdirectory)
          
    # Split the text to say into chunks of 100 characters since that is the max amount google will handle.
    for x in split_by_n(text,100):
 
        filename = x + ".mp3"
        filepath = os.path.join(tempdirectory, filename)
        
        if os.path.isfile(filepath):
            print "File already found in temp folder, playing that one instead."
                       
        else:
            print "File hasn't been found. Attempting to get it from google now..."
            headers = { 'User-Agent' : 'Mozilla/5.0' }
            req = urllib2.Request("http://translate.google.com/translate_tts?tl=en&q=" + urllib.quote_plus(x) , None, headers)
            response = urllib2.urlopen(req)
     
            print "Attemping save..."
            output = open(filepath, 'w')
            output.write(response.read())
            output.close()
        
        print "Playing audio file now"
        p = Popen(["mpg321", "-q", filepath])
        p.communicate()
        print "Done playing audio file"


def split_by_n( seq, n ):
    #A generator to divide a sequence into chunks of n units.
    while seq:
        yield seq[:n]
        seq = seq[n:]