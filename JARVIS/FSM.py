class FiniteStateMachine:
    
    states = []
    currentState = None
    def __init__(self):
        #Add all the states to the list
        self.states.append(StateListen())
        self.states.append(StateIdle())
        
        #Sort on priority
        self.states.sort(key=lambda x: x.priority, reverse=False)
        
        #print self.states
        for state in self.states:
            print "Added state: " + state.name
            
    def Pulse(self):
        for state in self.states:
            if state.needToRun == True:    
                state.Enter()
                state.Run()
                state.Exit()

class State:
    
    needToRun = True
    def __init__(self, name, priority):
        self.name = name
        self.priority = priority
    
    def Enter(self):
        print "Enter"
    def Run(self):
        print "Run"
    def Exit(self):
        print "Exit"
        
        
class StateIdle(State):
    
    priority = 1
    name = "Idle"
    
    def __init__(self):
        State.__init__(self, self.name, self.priority)
        print "StateIdle init"
        
    def Run(self):
        magicword = raw_input("Enter the magic word: ")
        print (magicword)
        
class StateListen(State):
    
    priority = 2
    name = "Listen"
    
    def __init__(self):
        State.__init__(self, self.name, self.priority)
        #self.needToRun = False
        print "StateListen init"
        
    def Enter(self):
        print "StateListen Enter()"
        

    